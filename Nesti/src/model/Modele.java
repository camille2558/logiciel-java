/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;

/**
 *
 * @author camillevidal
 */
public class Modele {

    //il faut ouvrir une connection
    public static Connection startConnection() throws SQLException {
        String url = "jdbc:mysql://127.0.0.1:8889/nesti";
        String user = "admin1";
        String password = "admin";
        Connection co = null;
        try {
            co = DriverManager.getConnection(url, user, password);
            if (!co.isClosed()) {
                System.out.println("Connexion ok");
            }
        } catch (SQLException ex) {
            System.err.println("Exception :" + ex.getMessage());
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);
        }

        return co;
    }

    public static void closConnection(Connection co) {
        if (co != null) {
            try {
                co.close();
            } catch (SQLException ex) {
                Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
