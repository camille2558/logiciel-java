/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static model.ModelIngredient.lesCategories;
import nesti.Categorie;
import nesti.Ingredient;
import nesti.Nesti;
import nesti.Recette;

/**
 *
 * @author camillevidal
 */
public class ModelRecette {

    static public ArrayList<Recette> lesRecettes = new ArrayList<Recette>();
  
    
    // READ
    
     public static int selectAllRecette() {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "Select * from Recette";
            ResultSet resultat = declaration.executeQuery(query);
            lesRecettes = new ArrayList<Recette>();
            while (resultat.next()) {
          
                //on ajoute un objet recette dans la collection a partir des informations obtenues dans la BDD
                Recette uneRec = new Recette();
                uneRec.setNomRec(resultat.getString("nomRec"));
                uneRec.setIdRec(resultat.getInt("id"));
                uneRec.setDescription(resultat.getString("description"));
                lesRecettes.add(uneRec) ;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }
     
      public static void readAllRecipe() {
        try {
            Connection co = Modele.startConnection();
            Statement stmt = co.createStatement();
            String query = "Select * from Recette";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {
                Recette r = new Recette();
                r.setNomRec(resultat.getString("nomRec"));
                r.setIdRec(resultat.getInt("id"));
              
            }
            resultat.close();
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
   //CREATE 

    public static int createRecette(String nom, String description, String image, int idCat) {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "Insert into Recette(nomRec,description,idCat,image) VALUES('" + nom + "','" + description + "','" + idCat + "','" + image + "')";
            System.out.println(query);

            //Execution de la requÃªte
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }

    public static void selectRecette(String nom) {
        try {
            Connection co = Modele.startConnection();

            Statement stmt = co.createStatement();
            String query = "Select * from Recette where nom = " + nom;
            ResultSet resultat = stmt.executeQuery(query);
            if (resultat.next()) {
                System.out.println("Recette :");
                String name = resultat.getString("nom");
                String un = resultat.getString(1);
            }
            resultat.close();
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

   
    //pour recuperer l'id  de la recette Ã  partir du nom saisie
    public static int GetRecId(String nom) {
        int id = 0;
        for (Recette uneRecette : lesRecettes) {
            if (uneRecette.getNomRec().equals(nom)) {
                id = uneRecette.getIdRec();
                break;
            }
        }
        return id;
    }
    //pour recuperer un ingrédient à partir de Id
    public static Recette GetRecette(int id) {
    	Recette uneRec = new Recette();
        for (Recette uneRecette : lesRecettes) {
            if (uneRecette.getIdRec() == id) {
                uneRec = uneRecette;
            	break;
            }
        }
        return uneRec;
    }
    
    
    //DELETE
    
    public static int deleteLaRecette(int idRec){
          int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "DELETE FROM Recette WHERE id = '" + idRec  + "'";
           

            //Execution de la requÃªte
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Suppréssion réussie");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
      
    }
    
    
    public static int deleteContenuRecette(int idRec) {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "delete from ContenuRecette where idRec = "+idRec;
            System.out.println(query);

            //Execution de la requÃªte
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Suppression réussie !");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }

    public static int updateRecette(int idRec , String descript) {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "update Recette  SET description = '" + descript + "' WHERE id = '" +  idRec  + "'";
            System.out.println(query);

            //Execution de la requÃªte
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
        
    }

    
}
