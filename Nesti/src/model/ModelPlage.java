/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import static model.ModelCours.lesLieux;
import static model.ModelCours.lesPlages;
import static model.ModelIngredient.lesCategories;
import static model.ModelRecette.lesRecettes;
import nesti.Categorie;
import nesti.Cours;
import nesti.Cuisinier;
import nesti.Ingredient;

import nesti.*;

/**
 *
 * @author camillevidal
 */
public class ModelPlage {

    static public ArrayList<PlageHoraire> lesPlages = new ArrayList<PlageHoraire>();

    // READ
    public static void ReadPlages() {
        //permet l'ajout des attributs aux collections d'objets

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from PlageHoraire";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {
                PlageHoraire unePlage = new PlageHoraire();
                unePlage.setId(resultat.getInt("id"));
                unePlage.setPlageDebut(LocalTime.parse(resultat.getString("debut")));
                unePlage.setPlageFin(LocalTime.parse(resultat.getString("fin")));

                lesPlages.add(unePlage);

            }
            //resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static int getIdPlage(String debut, String fin) {
        int id = 0;
        System.out.println("debut " + debut + " fin " + fin);

        for (PlageHoraire laPlage : lesPlages) {

            if (laPlage.getPlageDebut().toString() == null ? debut == null : laPlage.getPlageDebut().toString().equals(debut)) {
                id = laPlage.getId();
                break;
            }
        }
        return id;

    }
}
