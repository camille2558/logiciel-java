package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;

import nesti.*;

public class ModelIngredient {
// je crÃ©e des collections d'objet manipulables

    static public ArrayList<Categorie> lesCategories = new ArrayList<Categorie>();
   
    static public ArrayList<Ingredient> lesIng = new ArrayList<Ingredient>();
    static public Ingredient[] unIng = new Ingredient[100];
    static public ArrayList<IngredientsRecettes> lesIngRec = new ArrayList();
    static public ArrayList<Conditionnement> lesCond = new ArrayList<Conditionnement>();
    static public Conditionnement[] unCond = new Conditionnement[100];

    //CREATE
    public static int creerIngredient(String nom, int idCat, String description, String image) {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "Insert into Ingredients(nom,idCategorie,description,images) VALUES('" + nom + "','" + idCat + "','" + image + "','" + description + "')";
            System.out.println(query);

            //Execution de la requÃªte
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }

    //READ
    public static int creerIngredientPourRecette(int idCond, int idIng, int idRec, String quantite) {
        System.out.println(" idCond "+idCond + " idIng "+ idRec +"quantite "+quantite);
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();

            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "INSERT INTO ContenuRecette (idRec, idIng, quantite, idCond) VALUES (?, ?, ?, ? )";

            PreparedStatement declaration = co.prepareStatement(query);
            declaration.setInt(1, idRec);
            declaration.setInt(2, idIng);
            declaration.setString(3, quantite);
            declaration.setInt(4, idCond);

            //Execution de la requÃªte
            retour = declaration.executeUpdate();
            System.out.println("retour " + retour);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }

    public static void ReadAllCategorie() {

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from categorie";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {
                Categorie uneCat = new Categorie();
                uneCat.setNom(resultat.getString("nom"));
                uneCat.setId(resultat.getInt("id"));
                lesCategories.add(uneCat);
            }
            //resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void ReadAllIngredients() {

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from Ingredients";
            try (ResultSet resultat = stmt.executeQuery(query)) {
                lesIng = new ArrayList<Ingredient>();
                while (resultat.next()) {
                    Ingredient unIng = new Ingredient();
                    unIng.setIdIng(resultat.getInt("idIng"));
                    unIng.setNom(resultat.getString("nom"));
                    unIng.setDescription(resultat.getString("description"));
                    //unIng.setImage(resultat.getString("image"));
                    lesIng.add(unIng);

                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void ReadAllConditionnement() {

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from Conditionnement";
            try (ResultSet resultat = stmt.executeQuery(query)) {
                int i = 0;
                while (resultat.next()) {
                    Conditionnement uneCond = new Conditionnement();
                    uneCond.setNom(resultat.getString("nom"));
                    uneCond.setId(resultat.getInt("id"));
                    lesCond.add(uneCond);

                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Update 
    public static int updateIngredient(int idIng , String descript){
         int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "update Ingredients SET description = '" + descript + "' WHERE idIng = '" +  idIng  + "'";
            System.out.println(query);

            //Execution de la requÃªte
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
        
    }
     public static int deleteIngredient(int idIng) {
         
          int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "DELETE FROM Ingredients WHERE idIng = '" + idIng  + "'";
            System.out.println(query);

            //Execution de la requÃªte
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
      
    }
    

    // Get les Id's en fonction des noms
    public static int GetCategorieId(String nom) {
        int id = 0;
        for (Categorie uneCategorie : lesCategories) {
            if (uneCategorie.getNom() == nom) {
                id = uneCategorie.getId();
                break;
            }
        }
        return id;
    }

    //pour recuperer l'id a partir du nom saisie
    public static int GetIngId(String nom) {
        int id = 0;
        for (Ingredient unIngredient : lesIng) {
            if (unIngredient.getNom() == null ? nom == null : unIngredient.getNom().equals(nom)) {
                id = unIngredient.getIdIng();
                System.out.println(id +"dans model");
                break;
            }
        }
        return id;
    }
    
    //pour recuperer un ingrédient à partir de Id, retourne un objet ingredient pour afficher dans les champs que l'on veut modifier
    public static Ingredient GetIngredient(int id) {
    	Ingredient unIng = new Ingredient();
        for (Ingredient unIngredient : lesIng) {
            if (unIngredient.getIdIng() == id) {
                unIng = unIngredient;
            	break;
            }
        }
        return unIng;
    }

    public static int GetCondId(String nom) {
        int id = 0;
        for (Conditionnement unCond : lesCond) {
            if (unCond.getNom() == nom) {
                id = unCond.getId();
                break;
            }
        }
        return id;
    }

    //recupere les ingredients pour la recette en fonction de l'id de la recette
    public static ArrayList getIngredientsDeRecettes(int idRec) {
        ArrayList<IngredientsRecettes> lesIngRec = new ArrayList();
        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select i.nom , c.nom , cr.quantite ,cr.idRec from "
                    + "Ingredients as i join ContenuRecette as cr on cr.idIng = i.idIng "
                    + "join Conditionnement as c on c.id = cr.idCond "
                    + "where cr.idRec = '" + idRec + "'  ";
            ;
            ResultSet resultat = stmt.executeQuery(query);

            int i = 0;
            while (resultat.next()) {
                // on instancie l'objet puis on ajoute ses attributs
                IngredientsRecettes unIngRec = new IngredientsRecettes();
                unIngRec.setIdRec(resultat.getInt("cr.idRec"));
                unIngRec.setNomIng(resultat.getString("i.nom"));
                unIngRec.setNomCond(resultat.getString("c.nom"));
                unIngRec.setQuantite(resultat.getString("cr.quantite"));
                //ajout dans le tableau pour creer le JTable
                // tableau d'objet de type IngrdidentsRecette
                lesIngRec.add(unIngRec);

                i++;

            }
            resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lesIngRec;
    }

    public static ArrayList getIngredientCategories(int idCat) {
        System.out.println(" idCat param "+idCat);
        ArrayList<Ingredient> lesIngCat = new ArrayList();
        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from \n"
                    + "Ingredients as i join Categorie as c on c.id = i.idCategorie \n"
                    + "left outer join Tarif as t on t.idIng = i.idIng\n"
                    + "left outer JOIN Conditionnement as cond on cond.id = t.idCond\n"
                    + "where i.idCategorie = '" + idCat + "'  ";

            ResultSet resultat = stmt.executeQuery(query);

            int i = 0;
            while (resultat.next()) {
                // on instancie l'objet puis on ajoute ses attributs
                Ingredient unIng = new Ingredient();
                unIng.setNom(resultat.getString("i.nom"));
                unIng.setNomCond(resultat.getString("cond.nom"));
                unIng.setDescription(resultat.getString("i.description"));
                
                unIng.setTarif(resultat.getFloat("t.tarif"));
                unIng.setDevise(resultat.getString("t.devise"));
                lesIngCat.add(unIng);
                i++;

            }
            resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lesIngCat;

    }

   

}
