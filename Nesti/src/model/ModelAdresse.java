/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import static model.ModelCours.lesLieux;
import static model.ModelIngredient.lesCategories;
import static model.ModelRecette.lesRecettes;
import nesti.Categorie;
import nesti.Cours;
import nesti.Cuisinier;
import nesti.Ingredient;

import nesti.*;

/**
 *
 * @author camillevidal
 */
public class ModelAdresse {

    static public ArrayList<Adresse> lesAdresses = new ArrayList<Adresse>();
    static public Adresse[] uneAdresse = new Adresse[100];
    static public ArrayList<Lieux> lesLieux = new ArrayList<Lieux>();
    static public Lieux lieu = new Lieux();

//CREATE
    public static int creerAdresse(String rue, String ville, int cp) {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "Insert into adresse(rue,cp,ville) VALUES('" + rue + "','" + cp + "','" + ville + "')";
            System.out.println(query);
            //Execution de la requête
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }

    public static int creerLieu(String nom, int idAdresse) {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "Insert into adresse Lieu(nomLieu,idAdresse) VALUES('" + nom + "','" + idAdresse + "')";
            System.out.println(query);
            //Execution de la requête
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }

    // READ
    public static void ReadAdresses() {
        //permet l'ajout des attributs aux collections d'objets

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from adresse";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {
                Adresse uneAdresse = new Adresse();
                uneAdresse.setId(resultat.getInt("id"));
                uneAdresse.setRue(resultat.getString("rue"));
                uneAdresse.setCp(resultat.getInt("cp"));
                uneAdresse.setVille(resultat.getString("ville"));
                lesAdresses.add(uneAdresse);

            }
            //resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void readLieux() {
        //permet l'ajout des attributs aux collections d'objets

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from Lieux";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {

                lieu.setIdLieux(resultat.getInt("idLieux"));
                lieu.setNomLieux(resultat.getString("nomLieu"));
                lesLieux.add(lieu);

            }
            //resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Update
    //Delete
    //GET LES ID'S EN FONCTION DU NOM
    public static int getIdAdresse(String rue, String ville) {
        int id = 0;
        for (Adresse uneAdresse : lesAdresses) {
            if (uneAdresse.getRue() == rue && uneAdresse.getVille() == ville) {
                id = uneAdresse.getId();
                break;
            }
        }
        return id;

    }
// Get des id des Adresses en fonction du

    public static int GetAdresseId(String nomVille) {
        int id = 0;
        for (Adresse a : lesAdresses) {
            if (a.getVille() == null ? nomVille == null : a.getVille().equals(nomVille)) {
                id = a.getId();
                System.out.println(id);
                break;
            }
        }
        return id;
    }
// fonction pour remplir la JTable des cours

    public static ArrayList<Lieux> getLieuxAdresse(String nomVille) {
        ArrayList<Lieux> lesLieuxAdresse = new ArrayList();
        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from "
                    + "Adresse as a join Lieux as l on a.id = l.idAdresse "
                    + "where ville = '" + nomVille + "'  ";
            ;
            ResultSet resultat = stmt.executeQuery(query);

            int i = 0;
            while (resultat.next()) {
                // on instancie l'objet puis on ajoute ses attributs
                Lieux unLieuAdresse = new Lieux();
                unLieuAdresse.setNomLieux(resultat.getString("nomLieu"));
                unLieuAdresse.setRue(resultat.getString("rue"));
                unLieuAdresse.setCp(resultat.getInt("cp"));

                //ajout dans le tableau pour creer le JTable
                // tableau d'objet de type IngrdidentsRecette
                lesLieuxAdresse.add(unLieuAdresse);

                i++;

            }
            resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lesLieuxAdresse;
    }
    //recupère l'id du lieux avec avec le nom du lieu

   

}
