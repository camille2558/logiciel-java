/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import static model.ModelAdresse.lesLieux;
import static model.ModelIngredient.lesCategories;
import static model.ModelRecette.lesRecettes;
import nesti.Categorie;
import nesti.Cours;
import nesti.Cuisinier;
import nesti.Ingredient;

import nesti.*;

/**
 *
 * @author camillevidal
 */
public class ModelCours {

    static public ArrayList<Cours> lesCours = new ArrayList<Cours>();
    static public Cours[] unCours = new Cours[100];
    static public ArrayList<Cuisinier> lesCuisiniers = new ArrayList<Cuisinier>();

    static public ArrayList<Lieux> lesLieux = new ArrayList<Lieux>();
    static public ArrayList<PlageHoraire> lesPlages = new ArrayList<PlageHoraire>();
    static public PlageHoraire unePlage = new PlageHoraire();

    //CREATE 
    public static int creerCours(int idRec, int idCuis, int idLieux, int idPlageHoraire, String date) {
        System.out.println(idRec + " cuis " + idCuis + " lieu " + idLieux + " plage " + idPlageHoraire + " date " + date);
        int retour = 0;
        try {

            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "Insert into Cours(idRec,idCuisinier,idLieux,idPlageHoraire,date) VALUES('" + idRec + "','" + idCuis + "','" + idLieux + "','" + idPlageHoraire + "','" + date + "')";
            System.out.println(query);
            //Execution de la requête
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;
    }

    //READ
    public static void ReadAllCours() {

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from Cours ";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {

                Cours unCour = new Cours();
                unCour.setId(resultat.getInt("idCour"));
                unCour.setIdRec(resultat.getInt("idRec"));
                unCour.setIdCuis(resultat.getInt("idCuisinier"));
                unCour.setIdLieux(resultat.getInt("idLieux"));
                unCour.setIdPlageHoraire(resultat.getInt("idPlageHoraire"));
                //unCour.setDate(resultat.getSring("idPlageHoraire"));

                //unCuisinier.getUneSpe().setNom(resultat.getString("s.specialite"));
                lesCours.add(unCour);
            }
            //resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void ReadAllCuisinier() {

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from Utilisateur as u join Cuisinier as c on u.id = c.idCuisinier join Specialite as s on s.id = c.idSpecialite";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {

                Cuisinier unCuisinier = new Cuisinier();
                unCuisinier.setId(resultat.getInt("u.id"));
                unCuisinier.setNom(resultat.getString("u.nom"));
                unCuisinier.setPrenom(resultat.getString("u.prenom"));

                //unCuisinier.getUneSpe().setNom(resultat.getString("s.specialite"));
                lesCuisiniers.add(unCuisinier);
            }
            //resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void ReadAllLieux() {

        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "Select * from Lieux as l join Adresse as a on l.idAdresse = a.id ";
            ResultSet resultat = stmt.executeQuery(query);
            while (resultat.next()) {

                Lieux unlieu = new Lieux();
                unlieu.setNomLieux(resultat.getString("l.nomLieu"));
                unlieu.setIdLieux(resultat.getInt("l.idLieux"));
                unlieu.setRue(resultat.getString("a.rue"));
                unlieu.setCp(resultat.getInt("a.cp"));
                unlieu.setVille(resultat.getString("a.ville"));

                //unCuisinier.getUneSpe().setNom(resultat.getString("s.specialite"));
                lesLieux.add(unlieu);
            }
            //resultat.close();

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //GET LES ID'S EN FONCTION DES NOMS
    //pour recuperer l'id  de la recette à partir du nom saisie
    public static int GetCuisId(String nom) {

        int id = 0;
        for (Cuisinier unCuisiner : lesCuisiniers) {
            System.out.println(" getcuisid" + unCuisiner.getNom());
            if (unCuisiner.getNom() == null ? nom == null : unCuisiner.getNom().equals(nom)) {
                id = unCuisiner.getId();
                System.out.println(" dans model idcuis " + id);
                break;
            }
        }
        return id;
    }

    public static int getIdLieux(String nomLieu) {

        int id = 0;
        for (Lieux unLieu : lesLieux) {
            System.out.println(" coucou for");
            if (unLieu.getNomLieux().equals(nomLieu)) {
                System.out.println(" coucou if");
                id = unLieu.getIdLieux();
                System.out.println(" idlieu dans model " + id);
                break;
            }
        }
        return id;

    }

    public static ArrayList<Cours> getCourParCuisinier(int idCuis) {
        ArrayList<Cours> cours = new ArrayList();
        try {
            Connection co = Nesti.getCo();
            Statement stmt = co.createStatement();
            String query = "SELECT * FROM Cours as c JOIN Recette as r on c.idRec=r.id JOIN PlageHoraire as p on p.id = c.idPlageHoraire "
                    + "join Lieux as l on l.idLieux = c.idPlageHoraire where c.idCuisinier = '" + idCuis + "'";
            
            try (ResultSet resultat = stmt.executeQuery(query)) {
                int i = 0;
                while (resultat.next()) {
                    // on instancie l'objet puis on ajoute ses attributs
                    unePlage.setPlageDebut(LocalTime.parse(resultat.getString("debut")));
                    unePlage.setPlageFin(LocalTime.parse(resultat.getString("fin")));
                    Cours unCour = new Cours();
                    unCour.setNomRec(resultat.getString("r.nomRec"));
                    unCour.setIdRec(resultat.getInt("r.id"));
                    unCour.setPlage(unePlage);
                    unCour.setNomLieu(resultat.getString("l.nomLieu"));
                    lesCours.add(unCour);
                    
                    i++;
                    
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(ModelRecette.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lesCours;
    }

    public static int delCours(int idRec, int idCuis) {

        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "DELETE FROM Cours WHERE idRec = '" + idRec + "' and idCuisinier = '" + idCuis + "'";
            System.out.println(query);

            //Execution de la requête
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;

    }

    public static int getIdRec(String nomRec) {
        int id = 0;
        for (Cours c : lesCours) {
            System.out.println("c.getNomRec " + c.getNomRec());
            System.out.println(" nom param " + nomRec);
            if (c.getNomRec() == null ? nomRec == null : c.getNomRec().equals(nomRec)) {
                id = c.getIdRec();
                break;
            }
        }
        return id;
    }

    public static int modifCours(int idCour, int idRec, int idCuis, int idLieux, int idPlageHoraire, String date) {
        int retour = 0;
        try {
            //on ouvre la connection
            Connection co = Nesti.getCo();
            Statement declaration = co.createStatement();
            //requete
            //il faut mettre des simples quotes autout du nom
            String query = "update Cours SET idRec = '" + idRec + "',idCuisinier = '" + idCuis + "',idLieux='" + idLieux + "',idPlageHoraire ='" + idPlageHoraire + "',date = '" + date + "' WHERE idCour = '" + idCour + "'";
            System.out.println(query);

            //Execution de la requête
            retour = declaration.executeUpdate(query);
            //Traitement de l'insertion
            if (retour == 1) {
                System.out.println("Succes");
            } else {
                System.err.println("Echec");
            }
            //On ferme la connexion 
            //Modele.closConnection(co);

        } catch (SQLException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);

        }
        return retour;

    }

}
