/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structure;

/**
 *
 * @author camillevidal
 */
public class Recette {

    private int id;
    private String nom;

    public int getId() {
        return id;
    }
    public String getnom() {
        return nom;
    }
    

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override //surcharge
    public String toString() {
        return "Recette{" + "id=" + id + ", nom=" + nom + '}';
    }
    

}
