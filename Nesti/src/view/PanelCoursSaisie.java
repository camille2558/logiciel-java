/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évenements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

import nesti.*;
import model.*;

public class PanelCoursSaisie extends JPanel {

    private JComboBox fieldRecette;
    private JComboBox fieldCuisinier;
    private JTextField fieldLieux;
    String fin;
    String debut;

    private JTextField fieldCp;
    private JTextField fieldRue;
    private JTextField fielVille;
    public DefaultTableModel tableau;
    private int idLieu;

    //String valeur recupere la valeur d'une cellule du tableau
    private String valeur;
    private Cours cour = new Cours();
    private Cuisinier cuisinier = new Cuisinier();
    private Recette recette = new Recette();

    PanelCoursSaisie() {
        this.fieldRecette = fieldRecette;
        this.fieldCuisinier = fieldCuisinier;
        this.fieldLieux = fieldLieux;

        this.setBackground(Color.white);
        //permet l'affichage avec retour é  la ligne
        this.setLayout(null);
        //Jlabel : permet de mettre un text statique
        JLabel title = new JLabel("<html><h3>Fiche de gestion des Cours</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        // ComboBox pour séléctionner les Recettes
        JComboBox<String> comboRecette = new JComboBox<String>();
        comboRecette.addItem("Selectionnez");
        for (Recette r : ModelRecette.lesRecettes) {
            // parcours de la collection de recettes

            comboRecette.addItem(r.getNomRec());
            //on récupere les noms des recettes
        }
        comboRecette.setBounds(170, 60, 200, 20);
        this.add(comboRecette);

        JLabel labelCuisinier = new JLabel("Cuisinier");
        labelCuisinier.setBounds(60, 90, 200, 20);
        this.add(labelCuisinier);
        JComboBox<String> comboCuisinier = new JComboBox<String>();
        comboCuisinier.addItem("Selectionnez");
        for (Cuisinier c : ModelCours.lesCuisiniers) {

            comboCuisinier.addItem(c.getNom());
        }

        comboCuisinier.setBounds(170, 90, 200, 20);
        this.add(comboCuisinier);

        JLabel labelDate = new JLabel("Date");
        labelDate.setBounds(60, 150, 100, 20);
        this.add(labelDate);

        JTextField fieldJour = new JTextField();
        fieldJour.setBounds(170, 150, 40, 20);
        this.add(fieldJour);

        // ajout de slash entre les champs date
        JLabel labelSlash1 = new JLabel("/");
        labelSlash1.setBounds(220, 150, 30, 20);
        this.add(labelSlash1);

        JTextField fieldMois = new JTextField();
        fieldMois.setBounds(250, 150, 40, 20);
        this.add(fieldMois);

        JLabel labelSlash2 = new JLabel("/");
        labelSlash2.setBounds(300, 150, 30, 20);
        this.add(labelSlash2);

        JTextField fieldAnnee = new JTextField();
        fieldAnnee.setBounds(330, 150, 40, 20);
        this.add(fieldAnnee);

        // champ plage horaire
        JLabel labelPlage = new JLabel("Plage Horaire");
        labelPlage.setBounds(60, 180, 100, 20);
        this.add(labelPlage);
        JComboBox<String> comboPlage = new JComboBox<String>();
        comboPlage.addItem("Selectionnez");
        for (PlageHoraire p : ModelPlage.lesPlages) {

            comboPlage.addItem(p.getPlageDebut().toString() + "-" + p.getPlageFin().toString());

        }

        comboPlage.setBounds(170, 180, 200, 20);
        this.add(comboPlage);

        //Champs lieux
        JLabel labelLieux = new JLabel("Ville");
        labelLieux.setBounds(60, 210, 200, 20);
        this.add(labelLieux);
        JComboBox<String> comboLieux = new JComboBox<String>();
        comboLieux.addItem("Selectionnez");
        for (Lieux l : ModelCours.lesLieux) {

            comboLieux.addItem(l.getVille());
        }
        comboLieux.setBounds(170, 210, 200, 20);
        this.add(comboLieux);

        JLabel labelAdresse = new JLabel("Recette");
        labelAdresse.setBounds(60, 240, 200, 20);
        this.add(labelAdresse);

        // ComboBox pour séléctionner les Recettes
        JComboBox<String> comboAdresse = new JComboBox<String>();
        comboAdresse.addItem("Selectionnez");
        for (Adresse a : ModelAdresse.lesAdresses) {
            // parcours de la collection de recettes

            comboAdresse.addItem(a.getRue() + " " + a.getCp() + " " + a.getVille());

            //on récupere les noms des recettes
        }

        comboRecette.setBounds(170, 240, 200, 20);
        this.add(comboAdresse);


        //JTable 
        //ona besoin de l'objet DefaultTableModel pour remplir le tableau 
        tableau = new DefaultTableModel();

        JTable tableauAdresseLieu = new JTable(tableau);
        tableau.addColumn("Lieu");
        tableau.addColumn("Rue");
        tableau.addColumn("Code Postal");

        tableauAdresseLieu.setColumnSelectionAllowed(true);
        tableauAdresseLieu.setModel(tableau);

        tableau = (DefaultTableModel) tableauAdresseLieu.getModel();

        // ajout du scroll
        JScrollPane scrollPane = new JScrollPane(tableauAdresseLieu);
        this.add(scrollPane);
        scrollPane.setBounds(60, 330, 500, 200);

        // boutons
        JButton btnClear = new JButton("Réinitialiser");
        btnClear.setBounds(60, 550, 150, 25);
        this.add(btnClear);

        JButton btnAdd = new JButton("Enregistrer");
        btnAdd.setBounds(250, 550, 150, 25);//positionnement
        this.add(btnAdd);//ajout du boutton 


//        JButton btnModify = new JButton("Modifier");
//        btnModify.setBounds(60, 550, 150, 25);//positionnement
//        this.add(btnModify);//ajout du boutton 
//
//        JButton btnDel = new JButton("Supprimer");
//        btnDel.setBounds(250, 550, 150, 25);//positionnement
//        this.add(btnDel);//ajout du boutton 

 
        //evenement qui recupere le click sur une cellule du tableau
        tableauAdresseLieu.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {

                // clic sur le bouton gauche ou droit
                if (e.getButton() == MouseEvent.BUTTON1
                        || e.getButton() == MouseEvent.BUTTON3) {
                    int indRow = tableauAdresseLieu.rowAtPoint(e.getPoint());
                    int indCol = tableauAdresseLieu.columnAtPoint(e.getPoint());

                    // recupere la valeur 
                    valeur = tableauAdresseLieu.getValueAt(indRow, indCol).toString();
                    System.out.println(" valeur du lieu " + valeur);
                }
            }
        });
        // action enregistrer
        btnAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                if ((fieldAnnee.getText().isEmpty())) {
                    JOptionPane.showMessageDialog(null, "Il manque des données");

                } else {
                    //getSelectedItem est un objet donc on le transforme en string
                    String nomRec = comboRecette.getSelectedItem().toString();

                    int idRec = ModelRecette.GetRecId(nomRec);
                    cour.setIdRec(idRec);
                    System.out.println(" idRec " + idRec);

                    String nomCuisinier = comboCuisinier.getSelectedItem().toString(); //on récupere l'item selectionnée
                    int idCuis = ModelCours.GetCuisId(nomCuisinier); //on récupere l'id
                    cour.setIdCuis(idCuis); // on sette l'id dans l'objet

                    //les Plages horaires
                    String nomPlageHoraire = comboPlage.getSelectedItem().toString();
                    String sep = "-";
                    String plageCoupe[] = nomPlageHoraire.split(sep);
                    for (int i = 0; i < plageCoupe.length; i++) {
                        debut = plageCoupe[0];
                        fin = plageCoupe[1];
                    }

                    int idPlage = ModelPlage.getIdPlage(debut, fin);
                    cour.setIdPlageHoraire(idPlage);
                    String uneDate = fieldAnnee.getText() + "-" + fieldMois.getText() + "-" + fieldJour.getText();
                    cour.setDate(uneDate);
                    // lieu se trouvant dans le tableau et on recupere son id 
                    String nomLieuTableau = valeur;
                    idLieu = ModelCours.getIdLieux(valeur);
                    cour.setIdLieux(idLieu);
                    int retour = cour.enregistrerCours();
                    String message = "Données recette " + recette.getNomRec() + " enregistrées";
                    message = "données enregistrées";

                    if (retour == 0) {
                        JOptionPane.showMessageDialog(null, "Erreur lors de l'enregistrement");
                    } else {
                        recette.getLesIngredients(idRec);
                        JOptionPane.showMessageDialog(null, "Le cours a été ajouté avec succès !");
                        
                    }
                }
            }
        }
        );

        // action supprimer
//        btnDel.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent arg0) {
//                int selectRangee = tableauAdresseLieu.getSelectedRow();
//                tableau.removeRow(selectRangee);
//
//            }
//        }
//        );
        
        // Ajout d'une action sur la ComboBox des villes pour afficher les lieux
        comboLieux.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //on récupere la recette selectionnée 
                Object selectedLieux = comboLieux.getSelectedItem();
                // Transformation de l'objet en String
                String nomVille = selectedLieux.toString();

                //int id = ModelAdresse.GetAdresseId(nomVille);
                //System.out.println("idVille "+id);
                Object[] data = new Object[3];
                // on remplit le tableau 

                ArrayList<Lieux> lesLieux = ModelAdresse.getLieuxAdresse(nomVille);
                DefaultTableModel row = (DefaultTableModel) tableauAdresseLieu.getModel();

                if (row.getRowCount() != 0) {
                    // System.out.println("getRow"+row.getRowCount()); 
                    System.out.println(lesLieux);

                    row.setRowCount(0);
                    System.out.println("getRow" + row.getRowCount());
                }
                for (int i = 0; i < lesLieux.size(); i++) {
                    Lieux unLieux = lesLieux.get(i);
                    {
                        //on remplit les colonnes

                        data[0] = unLieux.getNomLieux();
                        data[1] = unLieux.getRue();
                        data[2] = unLieux.getCp();

                        row.addRow(data);
                    }

                }

            }

        });

    }

}
