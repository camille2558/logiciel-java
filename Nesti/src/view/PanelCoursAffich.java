/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évenements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import nesti.*;
import model.*;

public class PanelCoursAffich extends JPanel {

    DefaultTableModel tableau;

    String debut;
    String fin;
    String valeur;
    int idCuis;
    String message1;
    Cours leCour = new Cours();

    PanelCoursAffich() {

        this.setBackground(Color.white);
        //permet l'affichage avec retour Ã  la ligne
        this.setLayout(null);
        //Jlabel : permet de mettre un text statique
        JLabel title = new JLabel("<html><h3>Liste des cours par Cuisiniers</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        JLabel labelCuis = new JLabel("Les Cuisiniers");
        labelCuis.setBounds(50, 90, 200, 20);
        this.add(labelCuis);

        JComboBox<String> comboCuis = new JComboBox<String>();
        comboCuis.addItem("Selectionnez");
        for (Cuisinier c : ModelCours.lesCuisiniers) {

            comboCuis.addItem(c.getNom() + " " + c.getPrenom());
        }

        comboCuis.setBounds(210, 90, 200, 20);
        this.add(comboCuis);

        //JTable
        //ona besoin de l'objet DefaultTableModel pour remplir le tableau
        tableau = new DefaultTableModel();

        JTable tableauCategoriesIngredients = new JTable(tableau);
        // ajout des colonnes du tableau
        tableau.addColumn("Recette");
        tableau.addColumn("Plage Horaire");
        tableau.addColumn("Date");
        tableau.addColumn("Lieu");
        tableau.addColumn("Adresse");
        tableauCategoriesIngredients.setColumnSelectionAllowed(true);
        tableauCategoriesIngredients.setModel(tableau);

        tableau = (DefaultTableModel) tableauCategoriesIngredients.getModel();

        // ajout du scroll
        JScrollPane scrollPane = new JScrollPane(tableauCategoriesIngredients);
        this.add(scrollPane);
        scrollPane.setBounds(60, 200, 700, 200);

        JButton btnDel = new JButton("Supprimer");

        btnDel.setBounds(60, 500, 150, 25);//positionnement
        this.add(btnDel);//ajout du boutton 

        JButton btnModif = new JButton("Modifier");

        /**
         *
         * @param e
         */
        tableauCategoriesIngredients.addMouseListener(new MouseAdapter() {
            //evenement qui recupere le click sur une cellule du tableau
            @Override
            public void mousePressed(MouseEvent e) {

                // clic sur le bouton gauche ou droit
                if (e.getButton() == MouseEvent.BUTTON1
                        || e.getButton() == MouseEvent.BUTTON3) {
                    int indRow = tableauCategoriesIngredients.rowAtPoint(e.getPoint());
                    int indCol = tableauCategoriesIngredients.columnAtPoint(e.getPoint());

                    // recupere la valeur
                    valeur = tableauCategoriesIngredients.getValueAt(indRow, indCol).toString();

                }
            }
        });
        btnDel.addActionListener((ActionEvent arg0) -> {
            int selectRangee = tableauCategoriesIngredients.getSelectedRow();
            tableau.removeRow(selectRangee);
            // récuperation de l'ingredient séléctionnée dans la combobox
            String nomRec = valeur;
            System.out.println(" nomRec" + valeur);
            int idRec = ModelCours.getIdRec(nomRec);
            System.out.println(" idREc dans panel" + idRec);
            leCour.setIdRec(idRec);
            System.out.println(" idCuis " + idCuis);
            leCour.setIdCuis(idCuis); // récuperation de l'id de l'ingredient Ã  modifier

            int retour = leCour.deleteCour();
            if (retour == 1) {

                //ingredient.enregistrer();
                message1 = "Le cour Ã  bien été supprimé";

                JOptionPane.showMessageDialog(null, message1);

            } else {
                message1 = "Erreur lors de la supréssion ";
                JOptionPane.showMessageDialog(null, message1);
            }
        });

        //ajout d'une action sur la combobox des categories
        comboCuis.addActionListener((java.awt.event.ActionEvent evt) -> {
            //on récupere la recette selectionnée
            String nomPlageHoraire = comboCuis.getSelectedItem().toString();
            String sep = " ";

            String plageCoupe[] = nomPlageHoraire.split(sep);
            for (int i = 0; i < plageCoupe.length; i++) {
                debut = plageCoupe[0];

            }

            // Transformation de l'objet en String
            //on converti la categorie séléctionnée en id
            String nomCuis = debut;
            idCuis = ModelCours.GetCuisId(nomCuis);

            // remplissage du tableau
            Object[] data = new Object[4];
            // on remplit le tableau

            ArrayList<Cours> lesCours = ModelCours.getCourParCuisinier(idCuis);
            DefaultTableModel row = (DefaultTableModel) tableauCategoriesIngredients.getModel();

           
            for (int i = 0; i < lesCours.size(); i++) {
                Cours unCour = lesCours.get(i);
                {
                    //on remplit les colonnes
                    data[0] = unCour.getNomRec();
                    //data[1] = unCour.getPlage().getPlageDebut() + " h " + "-" + unCour.getPlage().getPlageFin() + " h";
                    data[1] = unCour.getDate();
                    data[2] = unCour.getNomLieu();

                    row.addRow(data);
                    if (row.getRowCount() != 0) {
                        row.setRowCount(0);
                    }
                }

            }
        });

    }

}
