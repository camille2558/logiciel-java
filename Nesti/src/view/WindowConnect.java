/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évènements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import model.ModelIngredient;
import nesti.*;
import java.util.Arrays;

/**
 *
 * @author camillevidal
 */
public class WindowConnect extends JFrame {

    private JPanel lePanel;
    private JTextField fieldLogin;
    private JPasswordField fieldMdp;
    private Utilisateurs unUtilisateur;
    private static boolean validConnect = false;

    public WindowConnect() {
        this.validConnect = validConnect;
        this.unUtilisateur = new Utilisateurs();
        //on rappelle les méthodes du parent JFrame
        this.setSize(600, 400); // taille de la fenetre
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE); //permet de fermer l'appli
        this.setLocationRelativeTo(null);
        this.setVisible(true); //rend visible l'interface
        this.setTitle("Login Nesti");

        JPanel lePanel = new JPanel();
        lePanel.setLayout(null);
        lePanel.setBackground(Color.white);
        lePanel.revalidate();//revalide le panneau lorsque on change de panneau 

        //ajout des éléments graphiques
        //titre pour la fenêtre
        JLabel title = new JLabel("<html><h3>Connexion</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        JLabel labelLogin = new JLabel("Nom utilisateur");//zone de texte pour decrire la zone de saisie
        labelLogin.setBounds(50, 60, 100, 20);//position de champs(x(horizontale),y(verticale)) + longueur de chaque champ
        lePanel.add(labelLogin);//on ajoute l'élément au panel

        fieldLogin = new JTextField();//zone de saisie
        fieldLogin.setBounds(160, 60, 120, 20); //ligne 60 comme le label
        lePanel.add(fieldLogin);

        JLabel labelMdp = new JLabel("Mot de passe");//zone de texte pour decrire la zone de saisie
        labelMdp.setBounds(60, 120, 120, 20);//position de champs(x(horizontale),y(verticale)) + longueur de chaque champ
        lePanel.add(labelMdp);//on ajoute l'élément au panel

        fieldMdp = new JPasswordField();//zone de saisie
        fieldMdp.setBounds(160, 120, 120, 20); //ligne 60 comme le label
        lePanel.add(fieldMdp);

        JButton btnCo = new JButton("Connexion");
        btnCo.setBounds(50, 180, 150, 25);//positionnement
        lePanel.add(btnCo);//ajout du boutton 

        this.setContentPane(lePanel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
        this.getContentPane();
        this.setVisible(true);

        btnCo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                connection();

            }

            public void WindowClosing(java.awt.event.WindowEvent evt) {

                System.exit(0);
            }

        }
        );
    }

    public void connection() {
        if ((fieldLogin.getText().isEmpty()) //c'est comme isset en php
                || (fieldMdp.getText().isEmpty())) // crée une pop up
        {
            JOptionPane.showMessageDialog(null, "Il manque des données");

        } else {
            // je get l'input puis je le transforme en integer
            unUtilisateur.setLogin(fieldLogin.getText());
            //il faut transformer l'objet en array puis en String
            char[] password = fieldMdp.getPassword();
            String mdp = Arrays.toString(password);
            unUtilisateur.setMdp(mdp);
            int reponse = unUtilisateur.checkLogin();

            //String message = "Données ingrédient " + ingredient.getNom() + " enregistrées";
            //message = "données enregistrées";
            if (reponse == 1) {
                JOptionPane.showMessageDialog(null, "Login/Mot de passe incorrect");
            } //else if (reponse == 2) {
            //JOptionPane.showMessageDialog(null, "Vous n'avez pas les droits pour accèder à Nesti");
            //}
            else {
                validConnect = true;
                System.out.println(validConnect);
                JOptionPane.showMessageDialog(null, "Bienvenue " + unUtilisateur.getLogin());
                this.setVisible(false);
                Window window = new Window();

                // pourquoi static ??
                //if( WindowConnect.getValidConnect() == true){
                //connect.WindowClosing(evt);
            }

        }

    }

    public static boolean getValidConnect() {
        return validConnect;

    }

    public static void main(String[] args) throws SQLException {

        // Interface graphique
        WindowConnect connect = new WindowConnect();

    }

    //}
}


