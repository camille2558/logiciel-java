package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évenements
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import nesti.*;
import model.*;
import view.*;

public class PanelIngredSaisie extends JPanel {
    //hérite de la classe jPanel qui apartient Ã  la bibliotheque swing
    //on aurait pu instancier l'objet jpanel aussi

    private JTextField fieldName;
    private JTextField fieldCategorie;
    private String message;
    private Ingredient ingredient = new Ingredient();
    public JComboBox comboRecette;
    public PanelIngredSaisie() {

    	this.setBackground(Color.WHITE);
        this.setLayout(null);
     
        JLabel title = new JLabel("<html><h3>Fiche de saisie ingrédient</h3></html>");
        title.setBounds(10, 10, 400, 20);
        this.add(title);
 
        JLabel labelNom = new JLabel("Nom");
        labelNom.setBounds(50, 50, 100, 20);
        this.add(labelNom);
        fieldName = new JTextField();
        fieldName.setBounds(150, 50, 150, 20);
        this.add(fieldName);

        
        JLabel labelCat = new JLabel("Catégories");
        labelCat.setBounds(50, 100, 150, 20);
        this.add(labelCat);
        JComboBox<String> comboCateg = new JComboBox<String>();
        comboCateg.addItem("Selectionnez");
        for (Categorie c : ModelIngredient.lesCategories) {

            comboCateg.addItem(c.getNom());
        }
        comboCateg.setBounds(150, 100, 200, 20);
        this.add(comboCateg);
        
        // Champ de saisie texte
        JLabel labelDescript = new JLabel("Description");
        labelDescript.setBounds(50, 150, 100, 20);
        this.add(labelDescript);
        
        //zone de text
        JTextArea fieldDescript = new JTextArea(0, 600);
        fieldDescript.setBounds(150, 150, 500, 200);
        this.add(fieldDescript);
        //ajout d'une bordure
        fieldDescript.setBorder(BorderFactory.createLineBorder(Color.black));
        this.add(fieldDescript);
        fieldDescript.setSize(300, 200);
        fieldDescript.setVisible(true);

        // Champ de saisie image
        JLabel labelImg = new JLabel("Image");
        labelImg.setBounds(50, 400, 100, 20);
        this.add(labelImg);
        //zone de text
        JTextField fieldImg = new JTextField();
        fieldImg.setBounds(150, 400, 300, 20);
        this.add(fieldImg);

        JButton btnClear = new JButton("Réinitialiser");
        btnClear.setBounds(50, 500, 150, 25);
        this.add(btnClear);

        JButton btnSave = new JButton("Enregistrer");
        btnSave.setBounds(250, 500, 150, 25);//positionnement
        this.add(btnSave);//ajout du boutton 

        btnSave.addActionListener((ActionEvent arg0) -> {

            if ((fieldName.getText().isEmpty()) || fieldDescript.getText().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Il manque des données");

            } //affiche une boite de dialogue
            else {
                // je get l'input puis je le transforme en integer
                ingredient.setDescription(fieldDescript.getText());
                ingredient.setImg(fieldImg.getText());
                ingredient.setNom(fieldName.getText());//get text recupere le texte
                //getSelectedItem est un objet donc on le transforme en string
                String nomCat = comboCateg.getSelectedItem().toString();
                int idCat = ModelIngredient.GetCategorieId(nomCat);
                ingredient.setIdCategorie(idCat);

                int retour = ingredient.enregistrerIngredient();
                if (retour == 1) {
                    
                    //ingredient.enregistrer();
                    String message1 = "Ingrédient " + ingredient.getNom() + " ajouté dans catégorie " + nomCat;
                    //message1 = "données enregistrées";
                    JOptionPane.showMessageDialog(null, message1);
                    fieldName.setText(null);
                    fieldDescript.setText(null);
                    fieldImg.setText(null);
                    ModelIngredient.ReadAllIngredients();
                   
                }
            }
        });

        btnClear.addActionListener(new ActionListener() { //pour réinitialiser les champs
            public void actionPerformed(ActionEvent e) {

                fieldName.setText(null);
                comboCateg.setSelectedItem("Produit frais");

            }
        });

    }
}

//PanelIngredSaisie.java
