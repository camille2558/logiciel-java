/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évÃ¨nements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import nesti.*;
import model.*;

public class PanelIngredAffich extends JPanel {

    DefaultTableModel tableau;

    PanelIngredAffich() {

        this.setBackground(Color.white);
        //permet l'affichage avec retour à la ligne
        this.setLayout(null);
        
        //Jlabel : permet de mettre un text statique
        JLabel title = new JLabel("<html><h3>Liste des ingredients disponibles</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        JLabel labelCat = new JLabel("Catégorie");
        labelCat.setBounds(50, 50, 100, 20);
        this.add(labelCat);

        JComboBox<String> comboCateg = new JComboBox<String>();
        comboCateg.addItem("Selectionnez");
        for (Categorie c : ModelIngredient.lesCategories) {

            comboCateg.addItem(c.getNom());
        }
        comboCateg.setBounds(210, 50, 200, 20);
        this.add(comboCateg);

        //JTable
        //ona besoin de l'objet DefaultTableModel pour remplir le tableau
        tableau = new DefaultTableModel();

        JTable tableauCategoriesIngredients = new JTable(tableau);
        // ajout des colonnes du tableau
        tableau.addColumn("Ingredients");
        tableau.addColumn("Description");
        tableau.addColumn("Conditionnement");
        tableau.addColumn("Prix");
        tableauCategoriesIngredients.setColumnSelectionAllowed(true);
        tableauCategoriesIngredients.setModel(tableau);

        
        tableau = (DefaultTableModel) tableauCategoriesIngredients.getModel();

        // ajout du scroll
        JScrollPane scrollPane = new JScrollPane(tableauCategoriesIngredients);
        this.add(scrollPane);
        scrollPane.setBounds(50, 100, 600, 300);

        //ajout d'une action sur la combobox des categories
        comboCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //on récupère la recette selectionnée 
                Object selectedCateg = comboCateg.getSelectedItem();
                // Transformation de l'objet en String
                
                //on converti la categorie séléctionnée en id
                String nomCateg = selectedCateg.toString();
                int idCateg = ModelIngredient.GetCategorieId(nomCateg);
                
                // remplissage du tableau
                Object[] data = new Object[4];
                ArrayList<Ingredient> lesIngredients = ModelIngredient.getIngredientCategories(idCateg);
                DefaultTableModel row = (DefaultTableModel) tableauCategoriesIngredients.getModel();

                if (row.getRowCount() != 0) {
                    row.setRowCount(0);
                }
                for (int i = 0; i < lesIngredients.size(); i++) {
                    Ingredient unIngredient = lesIngredients.get(i);
                    //on remplit les colonnes
                    data[0] = unIngredient.getNom();
                    data[1] = unIngredient.getDescription();
                    data[2] = unIngredient.getNomCond();
                    if (unIngredient.getTarif() != 0) {
                    	String tarif = String.format ("%.2f", unIngredient.getTarif());
                    	data[3] = tarif + " " + unIngredient.getDevise();
                    }
                    else
                    	data[3] = "";
                    row.addRow(data);
                }
            }
        });

    }


}
