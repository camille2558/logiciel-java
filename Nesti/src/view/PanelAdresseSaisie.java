package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évÃ¨nements
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import nesti.*;
import model.*;

public class PanelAdresseSaisie extends JPanel {
    //hérite de la classe jPanel qui apartient Ã  la bibliotheque swing
    //on aurait pu instancier l'objet jpanel aussi

    private JTextField fieldRue;
    private JTextField fieldCp;
    private JTextField fieldVille;
    private String message;
    private Adresse uneAdresse = new Adresse();

    public PanelAdresseSaisie() {

        this.setBackground(Color.WHITE);
        this.setLayout(null);
        JLabel title = new JLabel("<html><h3>Fiche de saisie adresse</h3></html>");
        title.setBounds(10, 10, 400, 20);
        this.add(title);

        //champ Rue
        JLabel labelRue = new JLabel("Rue");
        labelRue.setBounds(50, 80, 100, 20);
        this.add(labelRue);
        fieldRue = new JTextField();
        fieldRue.setBounds(150, 80, 150, 20);
        this.add(fieldRue);

        // champ cp
        JLabel labelCp = new JLabel("Code Postal");
        labelCp.setBounds(50, 120, 100, 20);
        this.add(labelCp);
        fieldCp = new JTextField();
        fieldCp.setBounds(150, 120, 150, 20);
        this.add(fieldCp);

        // Champ ville
        JLabel labelVille = new JLabel("Ville");
        labelVille.setBounds(50, 150, 100, 20);
        this.add(labelVille);
        //zone de text
        JTextField fieldVille = new JTextField();
        fieldVille.setBounds(150, 150, 150, 20);
        this.add(fieldVille);
        
        JLabel labelLieux = new JLabel("Lieu");
        labelLieux.setBounds(50, 190, 100, 20);
        this.add(labelLieux);
        
        JTextField fieldLieu = new JTextField();
        fieldLieu.setBounds(150, 190, 150, 20);
        this.add(fieldLieu);
        
        

        JButton btnAdd = new JButton("Enregistrer");
        btnAdd.setBounds(50, 250, 150, 25);//positionnement
        this.add(btnAdd);//ajout du boutton 

        btnAdd.addActionListener((ActionEvent arg0) -> {

            if ((fieldRue.getText().isEmpty()) || fieldCp.getText().isEmpty() || fieldVille.getText().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Il manque des données");

            } //affiche une boite de dialogue
            else {
                // je get l'input puis je le transforme en integer
                uneAdresse.setRue(fieldRue.getText());
                uneAdresse.setVille(fieldVille.getText());

                // on parse en int le contenu du JTextfield
                int cp = Integer.parseInt(fieldCp.getText());
                uneAdresse.setCp(cp);
                uneAdresse.setVille(fieldVille.getText());
                int retour = uneAdresse.enregistrerAdresse();
                uneAdresse.setIdLieu(ModelCours.getIdLieux(fieldLieu.getText()));
                int retour2 = uneAdresse.enregistrerLieu();
                if (retour == 1 || retour2 == 1) {

                    String message1 = "Données Adresse enregistrées";

                    JOptionPane.showMessageDialog(null, message1);
                    fieldRue.setText(null);
                    fieldCp.setText(null);
                    fieldVille.setText(null);
                    fieldLieu.setText(null);
                } else {

                    String message2 = "Echec de l'enregistrement";
                    JOptionPane.showMessageDialog(null, message2);

                }
            }
        });
    }
    
}

//PanelIngredSaisie.java
