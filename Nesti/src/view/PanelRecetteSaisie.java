/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évÃ¨nements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;

import nesti.*;
import model.*;

public class PanelRecetteSaisie extends JPanel {

    private JTextField fieldNom;
    private String categorie;
    private String description;
    private String image;
    private Ingredient ingredient = new Ingredient();
    private Recette recette = new Recette();

    PanelRecetteSaisie() {
        this.fieldNom = fieldNom;
        this.categorie = categorie;
        this.image = image;
        this.description = description;

        this.setBackground(Color.white);
        //permet l'affichage avec retour ligne
        this.setLayout(null);
        //Jlabel : permet de mettre un text statique
        JLabel title = new JLabel("<html><h3>Fiche de saisie Recette</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        JLabel labelNom = new JLabel("Nom");//zone de texte pour decrire la zone de saisie
        labelNom.setBounds(50, 60, 100, 20);//position de champs(x(horizontale),y(verticale)) + longueur de chaque champ
        this.add(labelNom);//on ajoute l'élément au panel
        fieldNom = new JTextField();//zone de saisie
        fieldNom.setBounds(150, 60, 600, 20); //ligne 60 comme le label
        this.add(fieldNom);

        JLabel labelCat = new JLabel("Catégories");
        labelCat.setBounds(50, 90, 100, 20);
        this.add(labelCat);

        JComboBox<String> comboCateg = new JComboBox<String>();
        comboCateg.addItem("Selectionnez");
        for (Categorie c : ModelIngredient.lesCategories) {

            comboCateg.addItem(c.getNom());
        }

        comboCateg.setBounds(150, 90, 200, 20);
        this.add(comboCateg);

        JLabel labelDescript = new JLabel("Description");
        labelDescript.setBounds(50, 150, 100, 20);
        this.add(labelDescript);
        //zone de text
        JTextArea fieldDescript = new JTextArea(0, 600);
        fieldDescript.setBounds(150, 150, 500, 200);
        //ajout d'une bordure
        fieldDescript.setBorder(BorderFactory.createLineBorder(Color.black));
        this.add(fieldDescript);
        fieldDescript.setSize(300, 300);
        fieldDescript.setVisible(true);

        JButton btnClear = new JButton("Réinitialiser");
        btnClear.setBounds(50, 500, 150, 25);
        this.add(btnClear);

        JButton btnSave = new JButton("Enregistrer");
        btnSave.setBounds(250, 500, 150, 25);//positionnement
        this.add(btnSave);//ajout du boutton 

        btnClear.addActionListener(new ActionListener() { //pour réinitialiser les champs
            public void actionPerformed(ActionEvent e) {
                fieldNom.setText(null);
                fieldDescript.setText(null);

                comboCateg.setSelectedItem("Selectionnez");

            }
        });

        btnSave.addActionListener((ActionEvent arg0) -> {
            if ((fieldNom.getText().isEmpty()) //c'est comme isset en php
                    || (fieldDescript.getText().isEmpty())) // crée une pop up
            {
                JOptionPane.showMessageDialog(null, "Il manque des données");

            } else {
                // je get l'input puis je le transforme en integer
                recette.setNomRec(fieldNom.getText());
                recette.setDescription(fieldDescript.getText());
                //getSelectedItem est un objet donc on le transforme en string
                String nomCat = comboCateg.getSelectedItem().toString();
                recette.setIdCat(ModelIngredient.GetCategorieId(nomCat));
                int retour = recette.enregistrer();
                System.out.println("retour " + retour);

                if (retour == 0) {

                    JOptionPane.showMessageDialog(null, "Erreur lors de l'enregistrement");

                } else {
                    //fieldNom.setText(null);
                    //fieldDescript.setText(null);
                    JOptionPane.showMessageDialog(null, "La recette a été crée avec succès !");
                    ModelRecette.selectAllRecette();
                }

            }
        });

    }

}
