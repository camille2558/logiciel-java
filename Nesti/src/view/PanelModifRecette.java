/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des événements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import nesti.*;
import model.*;

public class PanelModifRecette extends JPanel {

    DefaultTableModel tableau;
    private Recette recette = new Recette();

    PanelModifRecette() {

        this.setBackground(Color.white);
        //permet l'affichage avec retour à  la ligne
        this.setLayout(null);
        //Jlabel : permet de mettre un text statique
        JLabel title = new JLabel("<html><h3>Modifier une Recette</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        JLabel labelNom = new JLabel("Nom");
        labelNom.setBounds(50, 90, 200, 20);
        this.add(labelNom);

        JComboBox<String> comboRecette = new JComboBox<String>();
        comboRecette.addItem("Selectionnez");
        for (Recette r : ModelRecette.lesRecettes) {

            comboRecette.addItem(r.getNomRec());
        }

        comboRecette.setBounds(150, 90, 200, 20);
        this.add(comboRecette);

        // Champ de saisie texte
        JLabel labelDescript = new JLabel("Description");
        labelDescript.setBounds(50, 150, 100, 20);
        this.add(labelDescript);
        //zone de text
        JTextArea fieldDescript = new JTextArea(0, 600);
        fieldDescript.setBounds(150, 150, 500, 200);
        this.add(fieldDescript);
        //ajout d'une bordure
        fieldDescript.setBorder(BorderFactory.createLineBorder(Color.black));
        this.add(fieldDescript);
        fieldDescript.setSize(300, 300);
        fieldDescript.setVisible(true);

        JButton btnModif = new JButton("Modifier");
        btnModif.setBounds(250, 500, 150, 25);//positionnement
        this.add(btnModif);//ajout du boutton 

        JButton btnDel = new JButton("Supprimer");
        btnDel.setBounds(50, 500, 150, 25);//positionnement
        this.add(btnDel);//ajout du boutton 
        
        // action modifier
        btnModif.addActionListener((ActionEvent arg0) -> {

            if ((fieldDescript.getText().isEmpty())) {

                JOptionPane.showMessageDialog(null, "Il manque des données");

            } //affiche une boite de dialogue
            else {

                // récupération de l'ingredient séléctionnée dans la combobox
                String nomRec = comboRecette.getSelectedItem().toString();
                int idRec = ModelRecette.GetRecId(nomRec);
                recette.setIdRec(idRec); // récuperation de l'id de l'ingredient à  modifier

                // récuperation de la description 
                recette.setDescription(fieldDescript.getText());
                //String descript = ingredient.getDescription();
                //System.out.println(" descript "+descript);

                int retour = recette.modifRecette();
                if (retour == 1) {

                    //ingredient.enregistrer();
                    String message1 = "Données modifiés";
                    JOptionPane.showMessageDialog(null, message1);
                    //fieldDescript.setText(null);
                    ModelRecette.selectAllRecette();

                }
            }
        });
        
        // action supprimer
        btnDel.addActionListener((ActionEvent arg0) -> {

            // récupération de l'Recette séléctionnée dans la combobox
            String nomRec = comboRecette.getSelectedItem().toString();
            int idRec = ModelRecette.GetRecId(nomRec);
            recette.setIdRec(idRec); // récuperation de l'id de l'Recette a modifier

            int retour = recette.deleteRecette();
            if (retour == 1) {

                //ingredient.enregistrer();
                String message1 = "Recette supprimée";
                JOptionPane.showMessageDialog(null, message1);
                fieldDescript.setText(null);
                ModelRecette.selectAllRecette();

            }

        });
        // récupération recette sélectionnée et affichage des données
        comboRecette.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
           
                String nom = comboRecette.getSelectedItem().toString();
                int id = ModelRecette.GetRecId(nom);
                Recette uneRec = ModelRecette.GetRecette(id);
                fieldDescript.setText(uneRec.getDescription());
                
                //System.out.println("nom="+nom+"id="+id);
            }
        });

    }
}
