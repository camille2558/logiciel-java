package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import view.*;

public class Window {

    private PanelIngredSaisie panelIngredient;
    private PanelIngredientRecette panelIngredRecette;

    private JFrame frame = new JFrame();
    //jmenu pour les ingredients avec 2 sous menus
    private JMenuBar menuBar = new JMenuBar();

    private JMenu menuIngred = new JMenu("Ingrédients"); //elements constituant le menu
    private JMenuItem menuItemIngred01 = new JMenuItem("Fiche de saisie");//sous menus
    private JMenuItem menuItemIngred02 = new JMenuItem("Modifier ");
    private JMenuItem menuItemIngred03 = new JMenuItem("Liste par catégorie");
    //menu pour les recettes
    private JMenu menuRecette = new JMenu("Recettes");//nouvelle instance de constituant menu
    private JMenuItem menuItemRec01 = new JMenuItem("Fiche de saisie");
    private JMenuItem menuItemRec02 = new JMenuItem("Saisie des ingredients");
     private JMenuItem menuItemRec03 = new JMenuItem("Modifier recette");
    

    private JMenu menuCours = new JMenu("Cours de cuisine");
    private JMenuItem menuItemCours01 = new JMenuItem("Fiche de saisie");
    private JMenuItem menuItemCours02 = new JMenuItem("Liste de cours");
    private JMenuItem menuItemCours03 = new JMenuItem("Modifier");
    
    

    private JMenu menuAdresse = new JMenu("Gestion des adresses");
    private JMenuItem menuItemAdresse = new JMenuItem("Fiche de saisie");

    public Window() {
//concerne la fenetre principale
        frame.setSize(800, 700); // taille de la fenetre
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE); //permet de fermer l'appli
        frame.setLocationRelativeTo(null);
        frame.setVisible(true); //rend visible l'interface
        frame.setBackground(Color.white);
        frame.setTitle("Application NESTI");

        // construction sous-menu ingrédient      
        this.menuIngred.add(menuItemIngred01);
        this.menuIngred.add(menuItemIngred02);
        this.menuIngred.add(menuItemIngred03);
        // construction menu
        //ajout des sous menus
        this.menuBar.add(menuIngred);
        this.menuBar.add(menuRecette);//on ajoute l'objet
        this.menuBar.add(menuCours);

        this.menuBar.add(menuAdresse);
        this.menuRecette.add(menuItemRec01);
        this.menuRecette.add(menuItemRec02);
        this.menuRecette.add(menuItemRec03);
        
        this.menuCours.add(menuItemCours01);
        this.menuCours.add(menuItemCours02);
         this.menuCours.add(menuItemCours03);

        this.menuAdresse.add(menuItemAdresse);

        frame.setJMenuBar(menuBar);//met le menubar dans la fenetre(frame)
        frame.setVisible(true);
        //on ajoute les sous item 

        // Action sur les menus
        //action pour un item
        menuItemIngred01.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelIngredSaisie panel = new PanelIngredSaisie(); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });
        menuItemIngred03.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelIngredAffich panel = new PanelIngredAffich(); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });
          menuItemIngred02.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelModifIngred panel = new PanelModifIngred (); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });
        //Ã  chaque sous menu on appelle un panel (permet de rentrer les zones de saisies)
        menuItemRec01.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelRecetteSaisie panel = new PanelRecetteSaisie(); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });

        menuItemRec02.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                PanelIngredientRecette panel = new PanelIngredientRecette();
                frame.setContentPane(panel);
                panel.setVisible(true);
                panel.revalidate();
                // permet de voir les ingredients et recettes saisies
                panel.refreshIngredientsRecette();
                panel.refreshRecettes();
            }
        });
        
        menuItemRec03.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                PanelModifRecette panel = new PanelModifRecette();
                frame.setContentPane(panel);
                panel.setVisible(true);
                panel.revalidate();
                // permet de voir les ingredients et recettes saisies
               
            }
        });

        // on crée l'action sur le JMenuItem
        menuItemCours01.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelCoursSaisie panel = new PanelCoursSaisie(); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });
         // on crée l'action sur le JMenuItem
        menuItemCours02.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelCoursAffich panel = new PanelCoursAffich(); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });
         // on crée l'action sur le JMenuItem
        menuItemCours03.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelCoursModif panel = new PanelCoursModif(); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });
        

        // Action sur les menus
        //action pour un item
        menuItemAdresse.addActionListener(new ActionListener() {
            //recupere le click
            public void actionPerformed(ActionEvent arg0) {
                //panel : panneau dans lequel on peut ajouter les zones de saisie(ajout d'éléments graphiques)

                PanelAdresseSaisie panel = new PanelAdresseSaisie(); //nouvel objet panel
                frame.setContentPane(panel);//on associe le panel avec la fenetre en gros on met le panel dans la frame
                panel.setVisible(true);
                panel.revalidate();//revalide le panneau lorsque on change de panneau 

            }
        });

    }

    public void refreshComboBox() {

    }

}
