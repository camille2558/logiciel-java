/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évÃ¨nements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import nesti.*;
import model.*;

public class PanelModifIngred extends JPanel {

    DefaultTableModel tableau;
    private Ingredient ingredient = new Ingredient();

    PanelModifIngred() {

        this.setBackground(Color.white);
        //permet l'affichage avec retour Ã  la ligne
        this.setLayout(null);
        
        //Jlabel : permet de mettre un text statique
        JLabel title = new JLabel("<html><h3>Modifier un ingredient</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        // Combobox Nom
        JLabel labelNom = new JLabel("Ingrédient");
        labelNom.setBounds(50, 50, 200, 20);
        this.add(labelNom);

        JComboBox<String> comboIngred = new JComboBox<String>();
        comboIngred.addItem("Selectionnez");
        for (Ingredient i : ModelIngredient.lesIng) {

            comboIngred.addItem(i.getNom());
        }
        comboIngred.setBounds(150, 50, 200, 20);
        this.add(comboIngred);
        
        // Field Nom
        JLabel labelNom2 = new JLabel("Nom");
        labelNom2.setBounds(50, 100, 100, 20);
        this.add(labelNom2);
        JTextField fieldName = new JTextField();
        fieldName.setBounds(150, 100, 300, 20);
        this.add(fieldName);
        
        // Champ de saisie texte
        JLabel labelDescript = new JLabel("Description");
        labelDescript.setBounds(50, 150, 100, 20);
        this.add(labelDescript);
        //zone de text
        JTextArea fieldDescript = new JTextArea(0, 600);
        fieldDescript.setBounds(150, 150, 500, 200);
        this.add(fieldDescript);
        //ajout d'une bordure
        fieldDescript.setBorder(BorderFactory.createLineBorder(Color.black));
        this.add(fieldDescript);
        fieldDescript.setSize(300, 200);
        fieldDescript.setVisible(true);

        // Boutons
        JButton btnModif = new JButton("Enregistrer");
        btnModif.setBounds(50, 500, 150, 25);//positionnement
        this.add(btnModif);//ajout du boutton 
        
        JButton btnDel = new JButton("Supprimer");
        btnDel.setBounds(300, 500, 150, 25);//positionnement
        this.add(btnDel);//ajout du boutton 

        // récupération ingrédient sélectionné et affichage des données
        comboIngred.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //on récupère l'ingrédient selectionnée 
                Object selectedIng = comboIngred.getSelectedItem();
                //on convertit en id
                String nom = selectedIng.toString();
                int idIng = ModelIngredient.GetIngId(nom);
                
                // renvoi un objet ingredient à partir de l'id de l'ingredient séléctionné
                Ingredient unIng = ModelIngredient.GetIngredient(idIng);
                
                fieldName.setText(unIng.getNom());
                fieldDescript.setText(unIng.getDescription());
            }
        });
        
        // action bouton modifier
        btnModif.addActionListener((ActionEvent arg0) -> {

            if ((fieldDescript.getText().isEmpty())) {

                JOptionPane.showMessageDialog(null, "Il manque des données");

            } //affiche une boite de dialogue
            else {
               
                // récupÃ¨ration de l'ingredient séléctionnée dans la combobox
                String nomIng = comboIngred.getSelectedItem().toString();
                int idIng = ModelIngredient.GetIngId(nomIng);
                ingredient.setIdIng(idIng); // récuperation de l'id de l'ingredient Ã  modifier
                
                // récuperation de la description 
                ingredient.setDescription(fieldDescript.getText());
                //String descript = ingredient.getDescription();
                //System.out.println(" descript "+descript);

                int retour = ingredient.modifIngredient();
                if (retour == 1) {
                    String message1 = "Données ingrédient modifiées";
                    JOptionPane.showMessageDialog(null, message1);}
                    else{
                 	   JOptionPane.showMessageDialog(null, "Une erreur est survenue");
                     };
                      
                }
            
        });
        
        // suppression ingrédient
        btnDel.addActionListener((ActionEvent arg0) -> {
            
                // récupération de l'ingredient séléctionnée dans la combobox
                String nomIng = comboIngred.getSelectedItem().toString();
                int idIng = ModelIngredient.GetIngId(nomIng);
                ingredient.setIdIng(idIng); // récuperation de l'id de l'ingredient Ã  modifier
                
                int retour = ingredient.deleteIngredient();
                if (retour == 1) {
                    //ingredient.enregistrer();
                    String message1 = "Ingrédient supprimé";
                    JOptionPane.showMessageDialog(null, message1);
                    fieldName.setText(null);
                    fieldDescript.setText(null);
                    comboIngred.setSelectedItem("Selectionnez");}
                else{
                	   JOptionPane.showMessageDialog(null, "Une erreur est survenue");
                          	
                    };
                                
        });


    }
}
