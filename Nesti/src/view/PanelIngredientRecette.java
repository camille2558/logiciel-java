/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel; //éléments graphiques
import java.awt.Color; //objet graphique
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;//gestion des évÃ¨nements
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import nesti.*;
import model.*;

public class PanelIngredientRecette extends JPanel {

    private JTextField fieldQuantite;
    private String categorie;
    private String description;
    private String image;
    private IngredientsRecettes ingredient = new IngredientsRecettes();
    private Recette recette = new Recette();
    private int idRecette;
    public DefaultTableModel tableau;
    JComboBox<String> comboIngredients;
    JComboBox<String> comboRecette;

    PanelIngredientRecette() {
        this.fieldQuantite = fieldQuantite;
        this.categorie = categorie;
        this.image = image;
        this.description = description;
        this.idRecette = idRecette;

        this.setBackground(Color.white);
        //permet l'affichage avec retour Ã  la ligne
        this.setLayout(null);
        //Jlabel : permet de mettre un text statique
        JLabel title = new JLabel("<html><h3>Gestion des ingredients de recette</h3></html>");
        title.setBounds(10, 10, 400, 20); //position (x,y,longeur(x) , longueur(y)
        this.add(title);

        JLabel labelNom = new JLabel("Recette");//zone de texte pour decrire la zone de saisie
        labelNom.setBounds(60, 60, 200, 20);
        this.add(labelNom);

        // ComboBox pour séléctionner les Recettes ---------------------------------------//
        JComboBox<String> comboRecette = new JComboBox<String>();
        comboRecette.addItem("Selectionnez");
        for (Recette r : ModelRecette.lesRecettes) {

            comboRecette.addItem(r.getNomRec());
        }

        comboRecette.setBounds(170, 60, 200, 20);
        this.add(comboRecette);

        JLabel labelIngredients = new JLabel("Ingredients");
        labelIngredients.setBounds(60, 90, 200, 20);
        this.add(labelIngredients);

        JComboBox<String> comboIngredients = new JComboBox<String>();
        comboIngredients.addItem("Selectionnez");
        for (Ingredient i : ModelIngredient.lesIng) {
            comboIngredients.addItem(i.getNom());
        }
//        ModelIngredient.lesIng.forEach((i) -> {
//            comboIngredients.addItem(i.getNom());
//        });

        comboIngredients.setBounds(170, 90, 200, 20);
        this.add(comboIngredients);

        JLabel labelCond = new JLabel("Conditionnement");
        labelCond.setBounds(60, 120, 200, 20);
        this.add(labelCond);
        
        JComboBox<String> comboCond = new JComboBox<String>();
        comboCond.addItem("Selectionnez");
        for (Conditionnement c : ModelIngredient.lesCond) {

            comboCond.addItem(c.getNom());
        }

        comboCond.setBounds(170, 120, 200, 20);
        this.add(comboCond);

        JLabel labelQuantite = new JLabel("Quantité");
        labelQuantite.setBounds(60, 150, 100, 20);
        this.add(labelQuantite);
        JTextField fieldQuantite = new JTextField();
        fieldQuantite.setBounds(170, 150, 100, 20);
        this.add(fieldQuantite);
        
        JButton btnClear = new JButton("Réinitialiser");
        btnClear.setBounds(60, 200, 150, 25);
        this.add(btnClear);

        JButton btnAdd = new JButton("Ajouter");
        btnAdd.setBounds(250, 200, 150, 25);//positionnement
        this.add(btnAdd);//ajout du boutton 

        //JTable 
        //ona besoin de l'objet DefaultTableModel pour remplir le tableau 
        tableau = new DefaultTableModel();

        JTable tableauIngredientsRecette = new JTable(tableau);
        tableau.addColumn("Ingredients");
        tableau.addColumn("Conditionnement");
        tableau.addColumn("Quantité");
        tableauIngredientsRecette.setColumnSelectionAllowed(true);
        tableauIngredientsRecette.setModel(tableau);

        tableau = (DefaultTableModel) tableauIngredientsRecette.getModel();

        // ajout du scroll
        JScrollPane scrollPane = new JScrollPane(tableauIngredientsRecette);
        this.add(scrollPane);
        scrollPane.setBounds(60, 280, 500, 200);

//        JButton btnModify = new JButton("Modifier");
//        btnModify.setBounds(60, 530, 150, 25);//positionnement
//        this.add(btnModify);//ajout du boutton 

        JButton btnDel = new JButton("Supprimer");
        btnDel.setBounds(60, 530, 150, 25);//positionnement
        this.add(btnDel);//ajout du boutton 

        // action supprimer ingrédient
        btnDel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                int selectRangee = tableauIngredientsRecette.getSelectedRow();
                tableau.removeRow(selectRangee);
                //ModelRecette.deleteContenuRecette(idRec)

            }
        }
        );

        // action ajouter ingrédient
        btnAdd.addActionListener((ActionEvent arg0) -> {
            if ((fieldQuantite.getText().isEmpty())) //c'est comme isset en php) // crée une pop up
            {
                JOptionPane.showMessageDialog(null, "Il manque des données");

            } else {

                // je get l'input puis je le transforme en integer
                ingredient.setQuantite(fieldQuantite.getText());

                //getSelectedItem est un objet donc on le transforme en string
                String nomRec = comboRecette.getSelectedItem().toString();
                idRecette = ModelRecette.GetRecId(nomRec);
                ingredient.setIdRec(idRecette);
                System.out.println(" idRec " + idRecette + " " + nomRec);

                String nomCond = comboCond.getSelectedItem().toString();
                ingredient.setIdCond(ModelIngredient.GetCondId(nomCond));

                
                String nomIng = comboIngredients.getSelectedItem().toString();
                System.out.println("nomIng " + nomIng);
                
                int idIng = ModelIngredient.GetIngId(nomIng);
                System.out.println(" idIng " + idIng);
                ingredient.setIdIng(idIng);

                int retour = ingredient.enregistrerDansRecette();
   
                if (retour == 0) {
                    JOptionPane.showMessageDialog(null, "Erreur lors de l'enregistrement");
                } else {
                    JOptionPane.showMessageDialog(null, "Ingredient ajouté dans la recette ");
                    
                    recette.getLesIngredients(idRecette);
                    // on remplit le tableau 
                    Object[] data = new Object[3];
                    
                    ArrayList<IngredientsRecettes> lesIngredients = ModelIngredient.getIngredientsDeRecettes(idRecette);
                    DefaultTableModel row = (DefaultTableModel) tableauIngredientsRecette.getModel();

                    if (row.getRowCount() != 0) {
                        row.setRowCount(0);
                    }
                    for (int i = 0; i < lesIngredients.size(); i++) {
                        IngredientsRecettes unIngredient = lesIngredients.get(i);
                        data[0] = unIngredient.getNomIng();
                        data[1] = unIngredient.getNomCond();
                        data[2] = unIngredient.getQuantite();
                        row.addRow(data);
                    }
                    
                }

            }
        });

        // action sur la ComboBox des recettes : afficher ingrédients
        comboRecette.addActionListener((java.awt.event.ActionEvent evt) -> {
            //on récupère la recette selectionnée
            Object selectedRecette = comboRecette.getSelectedItem();
            // Transformation de l'objet en String
            String nomRec = selectedRecette.toString();
            System.out.println("selection " + nomRec);
            int id = ModelRecette.GetRecId(nomRec);

            // on remplit le tableau 
            Object[] data = new Object[3];
            
            ArrayList<IngredientsRecettes> lesIngredients = ModelIngredient.getIngredientsDeRecettes(id);
            DefaultTableModel row = (DefaultTableModel) tableauIngredientsRecette.getModel();

            if (row.getRowCount() != 0) {
                row.setRowCount(0);
            }
            for (int i = 0; i < lesIngredients.size(); i++) {
                IngredientsRecettes unIngredient = lesIngredients.get(i);
                data[0] = unIngredient.getNomIng();
                data[1] = unIngredient.getNomCond();
                data[2] = unIngredient.getQuantite();
                row.addRow(data);
            }
        });
        
        btnClear.addActionListener(new ActionListener() { //pour réinitialiser les champs
            public void actionPerformed(ActionEvent e) {

                fieldQuantite.setText(null);
                comboIngredients.setSelectedItem("Selectionnez");
                comboRecette.setSelectedItem("Selectionnez");
                comboCond.setSelectedItem("Selectionnez");
              }
        });

    }

    void refreshIngredientsRecette() {
        ModelIngredient.ReadAllIngredients();
        comboIngredients = new JComboBox<String>();
        comboIngredients.addItem("Selectionnez");
        for (Ingredient i : ModelIngredient.lesIng) {
            comboIngredients.addItem(i.getNom());
        }
        comboIngredients.setBounds(170, 90, 200, 20);
        this.add(comboIngredients);
    }

    void refreshRecettes() {
        System.out.println("refresh recette");
        ModelRecette.selectAllRecette();
        comboRecette = new JComboBox<String>();
        comboRecette.addItem("Selectionnez");
        for (Recette r : ModelRecette.lesRecettes) {

            comboRecette.addItem(r.getNomRec());
        }
        comboRecette.setBounds(170, 60, 200, 20);
        this.add(comboRecette);
    }

}
