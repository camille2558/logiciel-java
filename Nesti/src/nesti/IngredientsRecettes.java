/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nesti;

import model.ModelIngredient;
import static model.ModelIngredient.lesIng;

/**
 *
 * @author camillevidIngal
 */
public class IngredientsRecettes {

    private String nomIng;
    private String nomCond;
    private String quantite;
    private int idCond;
    private int idIng;
    private int idRec;

    public IngredientsRecettes() {
        this.nomIng = nomIng;
        this.nomCond = nomCond;
        this.quantite = quantite;
        this.idIng = idIng;
        this.idRec = idRec;
        this.idCond = idCond;
    }

    public int getIdCond() {
        return idCond;
    }

    public void setIdCond(int idCond) {
        this.idCond = idCond;
    }

    public int getIdIng() {
        return idIng;
    }

    public void setIdIng(int idIng) {
        this.idIng = idIng;
    }

    public int getIdRec() {
        return idRec;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public int getId() {
        return idIng;
    }

    public void setId(int idIng) {
        this.idIng = idIng;
    }

    public String getNomIng() {
        return nomIng;
    }

    public void setNomIng(String nomIng) {
        this.nomIng = nomIng;
    }

    public String getNomCond() {
        return nomCond;
    }

    public void setNomCond(String nomCond) {
        this.nomCond = nomCond;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }
    public String toString(){
        return nomIng;
    }
    
    public int enregistrerDansRecette() {
        int retour = ModelIngredient.creerIngredientPourRecette(this.idCond, this.idIng, this.idRec, this.quantite);
        return retour;

    }
   

}
