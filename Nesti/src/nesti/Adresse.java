/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nesti;

import model.ModelAdresse;
import model.ModelIngredient;

/**
 *
 * @author camillevidal
 */
public class Adresse {
    private int id;
    private String rue;
    private String ville;
    private int cp;
    private String lieu;
    private int idLieu;

    public Adresse() {
        this.id = id;
        this.rue = rue;
        this.ville = ville;
        this.cp = cp;
        this.lieu = lieu;
        this.idLieu = idLieu;
        
    }

    public int getIdLieu() {
        return idLieu;
    }

    public void setIdLieu(int idLieu) {
        this.idLieu = idLieu;
    }
    

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }
    
      public int enregistrerAdresse(){
        int retour=ModelAdresse.creerAdresse(this.rue, this.ville, this.cp) ;
        return retour;
     }
      
    public int enregistrerLieu(){
        int retour = ModelAdresse.creerLieu(this.lieu, this.id);
        return retour;
    }
    
    
}
