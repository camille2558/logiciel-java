/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nesti;

/**
 *
 * @author camillevidal
 */
public class Lieux {

    protected int idLieux;
    protected int idAdresse;
    protected String rue;
    protected int cp;

    protected String nomLieux;
    protected String ville;
    protected Cours cour; //agrégation avec classe Cours
    public Adresse uneAdresse = new Adresse();

    public Lieux() {
        this.idLieux = idLieux;
        this.idAdresse = idAdresse;
        this.nomLieux = nomLieux;
        this.rue = rue;
        this.cp = cp;
        this.ville = ville;

    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
    

    public int getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(int idAdresse) {
        this.idAdresse = idAdresse;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }
    

    public int getIdLieux() {
        return idLieux;
    }

    public void setIdLieux(int idLieux) {
        this.idLieux = idLieux;
    }

   
    public String getNomLieux() {
        return nomLieux;
    }

    public void setNomLieux(String nomLieux) {
        this.nomLieux = nomLieux;
    }

    public Cours getCour() {
        return cour;
    }

    public void setCour(Cours cour) {
        this.cour = cour;
    }

}
