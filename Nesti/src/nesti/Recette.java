/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nesti;
import model.*;

import java.sql.Connection;
import java.util.Date;
import static model.ModelIngredient.lesIng;

public class Recette {
    
    

    protected int idRec;
    protected String nomRec;
    protected String description;
    protected String img;
    protected int idCat;

    public Recette() {
        this.idRec = idRec;
        this.nomRec = nomRec;
        this.img = img;
        this.idCat = idCat;
        this.description=description;
    }

    public int getIdRec() {
        return this.idRec;
    }
     public String getDescription() {
        return this.description;
    }

    public String getNomRec() {
        return this.nomRec;
    }

    public String getImg() {
        return this.img;
    }

    public int getIdCat() {
        return this.idCat;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public void setNomRec(String nomRec) {
        this.nomRec = nomRec;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }
    
    
    
    
  
    public int enregistrer(){
        int retour=ModelRecette.createRecette(this.nomRec,this.description,this.img,this.idCat);
        return retour;
        
   }
    public void getLesIngredients(int idRec){
        ModelIngredient.getIngredientsDeRecettes(idRec);
    }

    public int modifRecette() {
       int retour=ModelRecette.updateRecette(this.idRec,this.description);
        return retour;
    }

    public int deleteRecette() {
        int retour=ModelRecette.deleteLaRecette(this.idRec);
        return retour;
       
    }

   

  

}
