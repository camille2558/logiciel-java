/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nesti;

import java.util.Date;
import model.ModelCours;

public class Cours {

 

    private int id;
    private int idRec;
    private int idLieux;
    private String nomLieu;
    private int idCuis;
    private String nomRec;
    private int idPlageHoraire;
    private PlageHoraire plage;
    
    private String date;

    public Cours() {
        this.id = id;
        this.nomRec = nomRec;
        this.idRec = idRec;
        this.idLieux = idLieux;
        this.idPlageHoraire = idPlageHoraire;
        this.date = date;
        this.idCuis = idCuis;
        this.nomLieu = nomLieu;
        this.plage = plage;
    }

    public String getNomRec() {
        return nomRec;
    }

    public void setNomRec(String nomRec) {
        this.nomRec = nomRec;
    }

    public String getNomLieu() {
        return nomLieu;
    }

    public void setNomLieu(String nomLieu) {
        this.nomLieu = nomLieu;
    }

    public PlageHoraire getPlage() {
        return plage;
    }

    public void setPlage(PlageHoraire plage) {
        this.plage = plage;
    }
    

    public int getIdCuis() {
        return idCuis;
    }

    public void setIdCuis(int idCuis) {
        this.idCuis = idCuis;
    }

    public int getId() {
        return id;
    }

    public int getIdRec() {
        return idRec;
    }

    public int getIdLieux() {
        return idLieux;
    }

    public int getIdPlageHoraire() {
        return idPlageHoraire;
    }

    public String getDate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public void setIdLieux(int idLieux) {
        this.idLieux = idLieux;
    }

    public void setIdPlageHoraire(int idPlageHoraire) {
        this.idPlageHoraire = idPlageHoraire;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int enregistrerCours() {
        int retour = ModelCours.creerCours(this.idRec, this.idCuis, this.idLieux, this.idPlageHoraire, this.date);
        return retour;
    }
   public int deleteCour() {
        int retour = ModelCours.delCours(this.idRec, this.idCuis);
        return retour;
    }

    public int modifierCours() {
        int retour = ModelCours.modifCours(this.id,this.idRec, this.idCuis, this.idLieux, this.idPlageHoraire, this.date);
        return retour;
       
    }
   

}
