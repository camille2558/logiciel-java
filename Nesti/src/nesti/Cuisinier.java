/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nesti;

public class Cuisinier extends Utilisateurs {
    private int idCuisinier;
    private Specialite uneSpe;
    private String prenom;
    

    public Cuisinier() {
        super();
        this.idCuisinier=idCuisinier;
        this.uneSpe = uneSpe;
        this.prenom= prenom;

    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    

    public int getIdCuisinier() {
        return idCuisinier;
    }

    public void setIdCuisinier(int idCuisinier) {
        this.idCuisinier = idCuisinier;
    }

    public Specialite getUneSpe() {
        return uneSpe;
    }

    public void setUneSpe(Specialite uneSpe) {
        this.uneSpe = uneSpe;
    }
    

    public int getId() {
        return idCuisinier;
    }

    public void setId(int idCuisinier) {
        this.idCuisinier = idCuisinier;
    }
    
    //assoc entre Specialite et Cuisinier -> unidirectionelle
  public Specialite getSpe(Specialite spe){ 
     return spe;
     }
  
    
  

}
