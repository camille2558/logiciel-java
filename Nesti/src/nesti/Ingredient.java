/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nesti;

import java.sql.Connection;
import model.ModelRecette;
import model.ModelIngredient;

/**
 *
 * @author camillevidal
 */
public class Ingredient {

    private String nom;
    private int idIng;
    private int idCategorie;
    private int idCond;
    private String nomCond;
    private int idRec;
    private String quantite;
    private String description;
    private double tarif;
    private String devise;

    private String img;

    public Ingredient() {
        this.nom = nom;
        this.idIng = idIng;
        this.idCategorie = idCategorie;
        this.idRec = idRec;
        this.quantite = quantite;
        this.idCategorie = idCategorie;
        this.img = img;
        this.description = description;
        this.nomCond = nomCond;
        this.tarif = tarif;
        this.devise = devise;

    }

    public double getTarif() {
        return tarif;
    }

    public void setTarif(double tarif) {
        this.tarif = tarif;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getNomCond() {
        return nomCond;
    }

    public void setNomCond(String nomCond) {
        this.nomCond = nomCond;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNom() {
        return this.nom;
    }

    public int getIdIng() {
        return this.idIng;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setIdIng(int id) {
        this.idIng = id;
    }

    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    public void setIdCond(int idCond) {
        this.idCond = idCond;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public int getIdCategorie() {
        return idCategorie;
    }

    public int getIdCond() {
        return idCond;
    }

    public int getIdRec() {
        return idRec;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setIdCat(int id) {
        this.idCategorie = idCategorie;
    }


    public int enregistrerIngredient() {

        int retour = ModelIngredient.creerIngredient(this.nom, this.idCategorie, this.img, this.description);
        return retour;

    }

  public int modifIngredient() {

        int retour = ModelIngredient.updateIngredient(this.idIng, this.description);
        return retour;

    }

    public int deleteIngredient() {
         int retour = ModelIngredient.deleteIngredient(this.idIng);
        return retour;
        
    }

}
