package nesti;

import java.sql.Connection;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.*;
import view.*;

public class Nesti {

    private static Connection co;

    public static Connection getCo() {
        if (co == null) {
            try {
                co = Modele.startConnection();
            } catch (SQLException ex) {
                Logger.getLogger(Nesti.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return co;
    }

    public static void main(String[] args) throws SQLException {

        // Connexion BD et chargement des données
        ModelIngredient.ReadAllCategorie();
        ModelRecette.selectAllRecette();
        ModelIngredient.ReadAllIngredients();
        ModelIngredient.ReadAllConditionnement();
        ModelCours.ReadAllCuisinier();
        ModelAdresse.ReadAdresses();
        ModelCours.ReadAllLieux();
        ModelPlage.ReadPlages();
        ModelCours.ReadAllCours();
        //ModelAdresse.readLieux();

        WindowConnect window = new WindowConnect();
    }

}

//Nesti.java
